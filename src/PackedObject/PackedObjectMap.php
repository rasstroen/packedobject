<?php
namespace bee\PackedObject;

class PackedObjectMap
{
    /**
     * Добавляем объект в коллекцию
     *
     * @param int $objectId
     * @param PackedObject $object
     */
    public function addObject(int $objectId, PackedObject $object):PackedObjectMap
    {

    }

    /**
     * Получаем объект
     * @param int $objectId
     */
    public function getObject(int $objectId)
    {

    }

    /**
     * Запаковываем массив объектов
     */
    public function pack():string
    {

    }
}