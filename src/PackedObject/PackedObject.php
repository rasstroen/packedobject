<?php
namespace bee\PackedObject;

use bee\PackedObject\Exception\IllegalMapException;
use bee\PackedObject\Exception\OutOfBoundsException;
use bee\PackedObject\Exception\UnknownFieldException;

class PackedObject
{
    const SIZE_2 = 1;// 0 or 1
    const SIZE_4 = 2;// up to 3
    const SIZE_8 = 3;// up to 7
    const SIZE_16 = 4;// up to 15
    const SIZE_32 = 5;// up to 31
    const SIZE_64 = 6;// up to 63
    const SIZE_128 = 7;// up to 127
    const SIZE_256 = 8;// up to 255
    const SIZE_512 = 9;// up to 511
    const SIZE_1024 = 10;// up to 1 023
    const SIZE_2048 = 11;// up to 2 047
    const SIZE_4096 = 12;// up to 4 095
    const SIZE_8192 = 13;// up to 8 191
    const SIZE_16k = 14; // up to 16 383
    const SIZE_32k = 15; // up to 32 767
    const SIZE_65k = 16; // up to 65 535
    const SIZE_131k = 17; // up to 131 071
    const SIZE_262k = 18; // up to 262 143
    const SIZE_524k = 19; // up to 524 287
    const SIZE_1m = 20; // up to 1 048 575
    const SIZE_2m = 21; // up to 2 097 151
    const SIZE_4m = 22; // up to 4 194 303
    const SIZE_8m = 23; // up to 8 388 607
    const SIZE_16m = 24; // up to 16 777 215
    const SIZE_33m = 25; // up to 33 554 431
    const SIZE_67m = 26; // up to 67 108 863
    const SIZE_134m = 27; // up to 134 217 727
    const SIZE_268m = 28; // up to 268 435 455
    const SIZE_536m = 29; // up to 536 870 911
    const SIZE_1b = 30; // up to 1 073 741 823
    const SIZE_2b = 31; // up to 2 147 483 647

    /**
     * @var array
     */
    private $map;

    /**
     * @var int
     */
    private $objectLength;

    /**
     * @var array
     */
    private $values;

    /**
     * @param array $map
     * @throws IllegalMapException
     */
    public function __construct(array $map)
    {
        if (empty($map)) {
            throw new IllegalMapException('empty map');
        }

        $totalObjectLength = 0;

        foreach ($map as $fieldName => &$length) {
            $length = (int) $length;

            if ($length < 1 || $length > self::SIZE_2b) {
                throw new IllegalMapException('illegal length for field "' . $fieldName . '"');
            }
            $this->map[$fieldName] = [
                'offset' => $totalObjectLength,
                'length' => $length,
                'absOffset' => $length + $totalObjectLength
            ];
            $totalObjectLength += (int) $length;
        }
        unset($length);
        if ($totalObjectLength > self::SIZE_2b) {
            throw new IllegalMapException('Out of bounds sum length for fields in map');
        }

        $this->objectLength = $totalObjectLength;
    }

    /**
     * @param string $field
     * @param int $value
     * @return PackedObject
     * @throws OutOfBoundsException
     * @throws UnknownFieldException
     */
    public function set(string $field, int $value):PackedObject
    {
        $this->checkFieldValue($field, $value);
        $this->values[$field] = $value;
        return $this;
    }

    private function checkFieldValue(string $fieldName, int $value):bool
    {
        if (!isset($this->map[$fieldName])) {
            throw new UnknownFieldException('field "' . $fieldName . '" is not in fields map');
        }

        $valueByteLength = $this->map[$fieldName]['length'];
        $valueMax = pow(2, $valueByteLength) - 1;

        if ($value > $valueMax) {
            throw new OutOfBoundsException('value for field "' . $fieldName . '" is out of bounds( got ' . $value . ', max ' . $valueMax . ')');
        }

        if ($value < 0) {
            throw new OutOfBoundsException('value for field "' . $fieldName . '" is out of bounds ( got ' . $value . ', min 0)');
        }
        return true;
    }

    /**
     * Заполняем объект данными из набора бит по маске
     *
     * @param string $bytes
     */
    public function fillFromBytes(string $bytes):PackedObject
    {

    }

    /**
     * Получаем значение поля
     * @param string $field
     */
    public function get(string $field):int
    {

    }

    /**
     * Получаем строку содержащую данные объекта в виде бит
     */
    public function getPacked():string
    {
        $mask = (1 << $this->objectLength) - 1;
        $result = 0;
        foreach ($this->map as $fieldName => $fieldSettings) {
            $result = ($result << $fieldSettings['length']) | $this->values[$fieldName];
        }
        return $result;
    }
}