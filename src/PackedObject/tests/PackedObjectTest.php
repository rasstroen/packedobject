<?php

namespace bee\PackedObject\tests;

use bee\PackedObject\PackedObject;

class PackedObjectTest extends \PHPUnit_Framework_TestCase
{
    public function testPackSingle()
    {
        $map = [
            'a' => PackedObject::SIZE_128,

        ];
        $object = new PackedObject($map);
        $object->set('a', 100);

        self::assertEquals('1100100', decbin($object->getPacked()));
    }

    public function testPackThree()
    {
        $map = [
            'a' => PackedObject::SIZE_128,
            'b' => PackedObject::SIZE_4,
            'c' => PackedObject::SIZE_4
        ];
        $object = new PackedObject($map);
        $object->set('a', 100);
        $object->set('b', 2);
        $object->set('c', 2);
        self::assertEquals('11001001010', decbin($object->getPacked()));
    }

    public function testPackMany()
    {
        $map = [
            'a' => PackedObject::SIZE_4,
            'b' => PackedObject::SIZE_1024,
            'c' => PackedObject::SIZE_4,
            'd' => PackedObject::SIZE_4,
            'e' => PackedObject::SIZE_4,
            'f' => PackedObject::SIZE_4,
        ];
        $object = new PackedObject($map);
        $object->set('a', 2);
        $object->set('b', 554);
        $object->set('c', 2);
        $object->set('d', 2);
        $object->set('e', 2);
        $object->set('f', 0);
        self::assertEquals('10100010101010101000', decbin($object->getPacked()));
        $object->set('f', 1);
        self::assertEquals('10100010101010101001', decbin($object->getPacked()));
        $object->set('f', 2);
        self::assertEquals('10100010101010101010', decbin($object->getPacked()));
        $object->set('f', 3);
        self::assertEquals('10100010101010101011', decbin($object->getPacked()));
        $object->set('b', 0);
        self::assertEquals('10000000000010101011', decbin($object->getPacked()));
    }

    /**
     * @expectedException \bee\PackedObject\Exception\IllegalMapException
     */
    public function testIllegalMapEmpty()
    {
        $map = [];
        new PackedObject($map);
    }

    /**
     * @expectedException \bee\PackedObject\Exception\IllegalMapException
     */
    public function testIllegalMapZeroFieldLength()
    {
        $map = ['field' => 0];
        new PackedObject($map);
    }

    /**
     * @expectedException \bee\PackedObject\Exception\IllegalMapException
     */
    public function testIllegalMapNegativeFieldLength()
    {
        $map = ['field' => -1];
        new PackedObject($map);
    }

    /**
     * @expectedException \bee\PackedObject\Exception\IllegalMapException
     */
    public function testIllegalMapOutOfBoundsFieldLength()
    {
        $map = ['field' => 33];
        new PackedObject($map);
    }

    /**
     * @expectedException \bee\PackedObject\Exception\IllegalMapException
     */
    public function testIllegalMapOutOfBoundsFieldsSumLength()
    {
        $map = ['field1' => 16, 'field2' => 16, 'field3' => 1];
        new PackedObject($map);
    }

    /**
     * @expectedException \bee\PackedObject\Exception\UnknownFieldException
     */
    public function testSetIllegalField()
    {
        $map = ['id' => PackedObject::SIZE_128];
        $object = new PackedObject($map);
        $object->set('unknownfield', 1);
    }

    /**
     * @expectedException \bee\PackedObject\Exception\OutOfBoundsException
     */
    public function testSetOutOfBounds()
    {
        $map = ['id' => PackedObject::SIZE_128];
        $object = new PackedObject($map);
        $object->set('id', 128);
    }

    /**
     * @expectedException \bee\PackedObject\Exception\OutOfBoundsException
     */
    public function testSetOutOfBoundsNegative()
    {
        $map = ['id' => PackedObject::SIZE_128];
        $object = new PackedObject($map);
        $object->set('id', -1);
    }
}
